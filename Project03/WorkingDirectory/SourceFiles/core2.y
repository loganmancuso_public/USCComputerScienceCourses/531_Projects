%{
/*****************************************************************
 * 'core2.y'
 * core2.y is a grammer specification file
 * tar cvfz project1.tgz   project1
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 04-10-2018--11:05:22
**/

yydebug=1;
#include <stdio.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include "kr.symtab.c"

#define GOTO 407
#define CODESIZE 1000

extern int *yylineno;


typedef struct node {
						int quadnum;
						struct node *link;
} *LIST, LISTNODE;

int opcode[CODESIZE];
char *op1[CODESIZE], *op2[CODESIZE], *target[CODESIZE];
int backpatch_verbose = 1;
char *VOID = "#VOID";

LIST tmplist;
LIST makelist(int);
LIST merge(LIST, LIST);

%}

%union {
	int type;
	int ival;
	char real;
	struct nlist  *place;
	struct {
		LIST  *true;
		LIST  *false;
	} list;
	int quad;
	LIST  next;
}

%token PROGRAM BEGINT INPUTT OUTPUTT ENDT /*core specific*/
%token IF THEN ENDIF ELSE WHILE LOOP ENDLOOP /*relational statements*/
%token DOUBLE FLOAT /*values*/
%type <place> LIST
%type <place> expression
%type <place> operand
%token <place> ID
%token <ival> INT
%token <real> REAL
%token <type> RELOP INTTYPE DOUBLETYPE FLOATTYPE REALTYPE /*types*/
%type <next> statement body other
%type <quad> marker
%type <list> boolean
%left AND 
%left NOT /*Bitwise Compare*/
%left OR
%token LPAREN RPAREN ASSOP BAR /*op*/
%left TIMES DIV
%left PLUS MINUS 
%token MOD /*math operations*/
%token LT GT EQT
%token SEMICOLON COLON DOT COMA DBQUOTE SQUOTE /*general*/
%token NEWLINE LIST /*whitespace*/

%%
/*program line in core*/
line:	program NEWLINE	{
							printf("recognized a program \n");
							yylineno++;
							exit(0);
						}
	;
/*$$		$1		$2		$3...	*/
program:	PROGRAM NEWLINE declaration BEGINT NEWLINE statement ENDT SEMICOLON
	;
statement:	body NEWLINE statement { $<next>$=$<next>1; yylineno++;}
	|	body NEWLINE {yylineno++;}
	|	if_statement
	;
/*core specific assignments*/
body:	input
	|	output
	|	assignment
	|	body marker statement {
								dl("$<next>1=", $<next>1); 
								printf("M.quad=%d; ", $2);
								dl("$<next>3=", $<next>3); 
								
								backpatch($<next>1,$2);
								$<next>$=$<next>3;
							}
	;
input:	INPUTT idlist SEMICOLON {yylineno++;}
	;
output:	OUTPUTT idlist SEMICOLON {yylineno++;}
	;
declaration: type idlist SEMICOLON NEWLINE declaration {yylineno++;}
	|	type idlist SEMICOLON NEWLINE {yylineno++;}
	;
idlist:	ID COMA idlist
	|	ID
	;
assignment:	ID ASSOP expression
	;
expression:	expression PLUS subtraction
	|	subtraction
	;
subtraction: subtraction MINUS term
	|	term
	;
term:	term TIMES divisor
	|	divisor
	;
divisor: divisor DIV factor
	|	factor
	;
factor:	LPAREN expression RPAREN	
	|	value
	|	ID
	;
if_statement:	IF boolean THEN marker statement ELSE other marker statement {yylineno++;}
	;
loop:	WHILE boolean LOOP statement ENDLOOP SEMICOLON
	;
operand:	value
	|	ID
	;
value:	INT
	|	DOUBLE
	|	FLOAT
	|	REAL
	;
type:	INTTYPE
	|	DOUBLETYPE
	|	FLOATTYPE
	|	REALTYPE
	;
boolean:	operand RELOP operand {
									gen($2, $1, $3, VOID);
									gen(GOTO, VOID, VOID, VOID);
									$$.true = makelist(nextquad -2);
									$$.false = makelist(nextquad -1);
									}
	|	boolean AND marker boolean{
											dl("\n$1.t=", $1.true); 
											dl("\n$1.f=", $1.false); 
											printf("\nM.quad=%d; ", $3);
											dl("\n$4.t=", $4.true); 
											dl("\n$4.f=", $4.false); 
											backpatch($1.true,$3);
											$$.true = $4.true;
											$$.false = merge($1.false, $4.false);
											dl("\n$$.t=", $$.true); 
											dl("\n$$.f=", $$.false); 
										}

	|	RPAREN boolean LPAREN {
									$$.true = $2.true;
									$$.false = $2.false;
								}
	;
marker:	{$$ = nextquad;}
	;
other:				{
					gen(GOTO, VOID, VOID, VOID);
			 		$$ = makelist(nextquad - 1);
				}
	;

%%

void gen(int op, char *p1, char *p2, char *r) {
    static int quadnumber = 0;

    quadnumber = quadnumber + 1;
    putchar('\n');
    printf("%d\t", quadnumber);
    switch (op){
    case PLUS:
              printf("PLUS\t");
           break;
    case TIMES:
              printf("TIMES\t");
           break;
    default:
           printf("Error in OPcode Field");
    }

    printf("%s\t", p1);
    printf("%s\t", p2);
    printf("%s\t", r);
}

void dl(char *label, LIST p){
    printf("%s", label);
    dumplist(p);
}

LIST makelist(int  q) {
   LIST    tmp;
   void    *malloc();
   tmp = (LISTNODE *) malloc(sizeof (LISTNODE));
   tmp -> quadnum = q;
   return(tmp);
}

void backpatch(LIST  p, int q) {
   if(backpatch_verbose)
     {printf("\nBackpatching Quads:");}

   while (p != NULL){
        if(backpatch_verbose) printf("%d, ", p->quadnum);
	target[p->quadnum] = (LISTNODE *) q;
	p = p -> link;
   }
   if(backpatch_verbose) printf("to %d\n", q);
}

LIST merge(LIST  p1, LIST p2) {
   LIST  tmp;
   tmp = p1;
   if (tmp == NULL) return(p2);
   while((tmp->link) != NULL){
	tmp = tmp -> link;
   }
   tmp -> link = p2;
   return(p1);
}

/****************************************************************
 * End 'core2.y'
**/
